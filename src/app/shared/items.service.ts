import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Response } from '@angular/http';
import { Item } from '../models/item.model';
import 'rxjs';

@Injectable()
export class ItemsService {

  itemsChanged = new Subject<Item[]>();
  items:any;

  constructor(private http: Http) {}

  getItemsRequest() {

	return this.http.get('http://demo4452328.mockable.io/forms')
					.map( 

					(res:Response)=> {

						return res.json();
						
						}
					)
					.catch(error => {

						return error.json();

					});

  }

  getItemRequest(id) {

	return this.http.get('http://demo4452328.mockable.io/forms/' + id)
					.map( 

					(res:Response)=> {

						return res.json();
						
						}
					)
					.catch(error => {

						return error.json();

					});

  }

  itemsReceived(items:Item[]) {

  	this.items = items;

  }

  deleteItem(index:any) {

		this.items.splice(index, 1);
		this.itemsChanged.next(this.items.slice());

  }

  returnItems() {

  	return this.items;

  }

 getItemOne(items:any,id:number) {

 	if(this.items == undefined) {

 		this.items = items;

 	}

	let index;

 	for(let i = 0; i < this.items.length; i++) {

 		if(this.items[i].id == id ) {

 			index = i;

 		}

 	}

	return this.items[index];

   }
	
}