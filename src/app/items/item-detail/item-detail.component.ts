import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ItemsService } from '../../shared/items.service';
import { Item } from '../../models/item.model';


@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {

  id:number;
  items;
  itemForm: FormGroup;
  itemData:any;
  item:any;
  editMode = false;
  editFormMode = false;
  maskDouble = [/^\d+/, '.', /^\d+/];


  constructor(private route: ActivatedRoute,
              private service: ItemsService,
              private router: Router) { }

  ngOnInit() {

  	this.route.params

  	 .subscribe(

    	 	(params:Params) => {

    	 		this.id = +params['id'];

               this.InitForm();
           
    	 	}

  	 );

  }


  changeInput(e) {
    console.log(e);
  }


  private InitForm() {

  	  let dataString = {'edit':'false',};
      let dataInt = "";
      let dataDouble = [/[^\d,]/g];
      let dataText = "";
      let dataBool = "";

      this.service.getItemRequest(this.id).subscribe(

      res => {

           this.itemData = res.data;

           this.editFormMode = true;

          },

      error =>  {

          console.log(error);

      });


      this.service.getItemsRequest().subscribe(

      res => {

           this.items = res.data;
           
           this.editMode = true;

           for(let i = 0; i<this.items.length; i++) {

             if(this.items[i].id == this.id) {

               this.item = this.items[i];

             }

           }

          },

      error =>  {

          console.log(error);

      });
  }

}
