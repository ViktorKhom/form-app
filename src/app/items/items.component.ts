import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { ItemsService } from '../shared/items.service';
import { Item } from '../models/item.model';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit, OnDestroy {

  private items:Item[];
  subscription:Subscription;

  constructor(private service:ItemsService,
              private router:Router,
              private route: ActivatedRoute) {}

  ngOnInit() {

      this.items = this.service.returnItems();

  		this.service.getItemsRequest().subscribe(

			res => {

           let arr = res.data;

			     for(let i = 0; i < arr.length; i++) {

               if(arr[i].description.split('').length > 78) {

                  arr[i].description = arr[i].description.split('').slice(0, 76).join('') + '... .';

               }

           }

           if(this.items == undefined) {

             this.items = arr;
             this.goToService(res.data);

           }
             
			    },

			error =>  {

	              console.log(error);

	    });

      this.subscription = this.service.itemsChanged.subscribe(

          (items: Item[]) => {

              this.items = items;
              
          }

      );

  }


  goToService(items:Item[]) {

     this.service.itemsReceived(items);

  }

  goToDetailItem(item) {

    this.router.navigate([item.id], {relativeTo:this.route});

  }

  onDeleteItem(index:any) {

    this.service.deleteItem(index);

  }

  ngOnDestroy() {

    this.subscription.unsubscribe();

  }

}
