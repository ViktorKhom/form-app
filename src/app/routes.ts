import { Routes } from '@angular/router';
import { ItemsComponent } from './items/items.component';
import { ItemDetailComponent } from './items/item-detail/item-detail.component';

export const appRoutes:Routes = [
	
	{path:"", redirectTo:"/items", pathMatch:'full'},
	{path:"items", component:ItemsComponent},
	{path:"items/:id", component:ItemDetailComponent}
	
]